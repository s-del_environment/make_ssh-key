# SSH Key の作成
- [Secure Shell - Wikipedia](https://ja.wikipedia.org/wiki/Secure_Shell)
- [OpenSSH - Wikipedia](https://ja.wikipedia.org/wiki/OpenSSH)
- [公開鍵暗号 - Wikipedia](https://ja.wikipedia.org/wiki/%E5%85%AC%E9%96%8B%E9%8D%B5%E6%9A%97%E5%8F%B7)
- [エドワーズ曲線デジタル署名アルゴリズム - Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%A8%E3%83%89%E3%83%AF%E3%83%BC%E3%82%BA%E6%9B%B2%E7%B7%9A%E3%83%87%E3%82%B8%E3%82%BF%E3%83%AB%E7%BD%B2%E5%90%8D%E3%82%A2%E3%83%AB%E3%82%B4%E3%83%AA%E3%82%BA%E3%83%A0)

OpenSSH の `ssh-keygen` コマンドを使用し、暗号化通信のための**秘密鍵と公開鍵のキーペア**を作成する。  
秘密鍵は接続元に保存し、公開鍵は接続先に保存する。  
キーペアは正しい組み合わせでなければ当然認証されない。  

公開鍵は漏洩しても全く問題ないが、秘密鍵は絶対に漏洩してはならない。  
秘密鍵が漏洩した場合はそのキーペアを削除し、新たに鍵を作成する。  

GitLab 等ならば、**PC 端末に秘密鍵を保存**し **GitLab に公開鍵を保存**する。  
コマンドラインや Gitクライアント[^Gitクライアント] からプッシュやフェッチなどのリモートリポジトリの操作を行う場合、  
ユーザー認証 (本人確認と権限確認) が必要になるが、この時、端末の秘密鍵と GitLab に登録してある公開鍵で認証する。


## 環境
以下の環境でキーペアの発行を行った
- Windows 10 Home (64bit 1909)
    - PowerShell 7.1.1

バージョン `1803` 以降の Windows 10 には OpenSSH が組み込まれているため、
別途ダウンロードやインストール作業をしなくても `ssh-keygen` コマンドは使用出来る。  
また、Git をインストールすれば OpenSSH も付いてくるので、  
`<Git インストール先>\bin` か `<Git インストール先>\usr\bin` に `ssh-keygen` が存在する。


## 鍵ファイルの作成手順
1. `cd <鍵の保存先>`
1. `ssh-keygen -t ed25519 -a <任意のラウンド回数> -C "<任意のコメント>" -P "<任意のパスフレーズ>" -f .\<任意の鍵名>`

`<任意の鍵名>` と `<任意の鍵名>.pub` の2ファイルが作成され、`.pub` が公開鍵。


## 参考
- [ssh-keygenコマンドの使い方 - Qiita](https://qiita.com/hana_shin/items/2003698873a5782b7efd)
- [public key - How many KDF rounds for an SSH key? - Cryptography Stack Exchange](https://crypto.stackexchange.com/questions/40311/how-many-kdf-rounds-for-an-ssh-key)


## 補足
[^Gitクライアント]: SourceTree や GitKraken など
